
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by shaoyifei on 2018/6/14.
 * 从1-1000的证书数组中使用二分查找随机查找一个整数，实现对象比较和toString接口的代理
 */
public class ProsyTest {
    public static void getNum(){
        Object[] elements = new Object[1000];
        for(int i = 0; i < elements.length; i++){
            //被代理的对象
            Integer value = i+1;
            //构建调度处理器
            InvocationHandler handler = new TranceHander(value);
            Object proxy = Proxy.newProxyInstance(null,new Class[]{Comparable.class},handler);
            elements[i] = proxy;
        }
        Integer key = new Random().nextInt(1000)+1;
        int res = Arrays.binarySearch(elements,key);
        if (res>=0){
            System.out.println(elements[res].toString());
        }
        System.out.println("查找key："+key);
    }

    public static void main(String[]args){
        getNum();
    }
}