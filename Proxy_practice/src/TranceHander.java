import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by shaoyifei on 2018/6/14.
 */
public class TranceHander implements InvocationHandler{

    private Object tObject;

    public TranceHander(Object tonject) {
        this.tObject = tonject;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("被代理的对象："+tObject);
        System.out.println("方法："+method.getName());
        return method.invoke(tObject,args);
    }
}