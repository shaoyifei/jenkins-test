/**
 * Created by shaoyifei on 2018/5/25.
 */
public class JavaBean {
    private static int age;
    private static String name;
    private static String describe;

    public static int getAge() {
        return age;
    }

    public static void setAge(int age) {
        JavaBean.age = age;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        JavaBean.name = name;
    }

    public static String getDescribe() {
        return describe;
    }

    public static void setDescribe(String describe) {
        JavaBean.describe = describe;
    }

    @Override
    public String toString() {
        return "name:"+name+"\n"+"age:"+age+"\n"+"des:"+describe;
    }
}
